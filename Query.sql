﻿
create extension postgis

-- select

CREATE OR REPLACE FUNCTION getApartosFromFincaArray(id int) RETURNS json[] AS $$
DECLARE
	arreglo json[]; 

	ax_id int;
	ax_geom geometry(MultiPolyGon,5367);


	cursor_uno CURSOR FOR
	SELECT gidfinca,geom FROM apartos;
BEGIN
	OPEN cursor_uno;
	FETCH cursor_uno INTO ax_id,ax_geom;
	WHILE found LOOP

		IF(ax_id=id) THEN
			arreglo = array_append(arreglo,to_json(ST_AsGeoJSON(ST_FlipCoordinates(ST_Transform(ST_CollectionHomogenize(ax_geom),4326)))));
		END IF;


		FETCH cursor_uno INTO ax_id,ax_geom;
	END LOOP;
	CLOSE cursor_uno;


	RETURN arreglo;
	
END;
$$ LANGUAGE plpgsql



select canton,
                      codigofinca,
                      direccionexacta,
                      distrito,
                      fecha,
                      gid,
                      nombrefinca,
                      provincia,
                      telefono,
                      to_json((SELECT  getApartosFromFincaArray(gid)))as geom
                  from fincas where iduser = 1



CREATE OR REPLACE FUNCTION config_union_from_apartos(datos varchar(5000)) RETURNS VOID AS $$
DECLARE
	omgjson json := datos;
	i int;
	auxGeom geometry(MultiPolygon,5367);
	gGidFinca int;
	toBuilt geometry[]; -- SELECT*FROM apartos
	finalGeom geometry(MultiPolygon,5367);
BEGIN
	FOR i IN SELECT * FROM json_array_elements(omgjson)
	LOOP
		auxGeom = (SELECT geom FROM apartos WHERE gid=i);
		--select auxGeom;
		toBuilt = array_append(toBuilt,auxGeom);

		gGidFinca = (SELECT gidFinca from apartos where gid=i);

		UPDATE apartos set estado=1 WHERE gid=i;


	END LOOP;


	finalGeom= (select ST_Multi(ST_Collect(ST_Union(toBuilt)))); 

	INSERT INTO apartos(gidfinca,estado,geom,idActividad) VALUES (gGidFinca,0,finalGeom,1);


END;
$$ LANGUAGE plpgsql


CREATE OR REPLACE FUNCTION builtArray(iduser int,provincia text,canton text,distrito text,direccionexacta text,codigofinca int ,telefono int,nombrefinca text, datos varchar(50000)) RETURNS VOID AS $$
DECLARE
    omgjson json := datos ; --
    i json;
    toBuild geometry[];
    lastReg int;
BEGIN
  FOR i IN SELECT * FROM json_array_elements(omgjson)
  LOOP
         toBuild = array_append(toBuild,(select ST_Multi(ST_GeomFromGeoJSON(''||i||''))));--array_append(toBuild,(select ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(''||i||''),4326),5367)))); 
  END LOOP;
	insert into fincas (iduser,provincia,canton,distrito,direccionexacta,codigofinca,telefono,nombrefinca,geom)
         VALUES
                   (
                       iduser,provincia,canton,distrito,direccionexacta,codigofinca,telefono,nombrefinca,
                       (select ST_Multi(ST_Collect(ST_Transform(ST_SetSRID(ST_Union(toBuild),4326),5367))))
                   );

	lastReg = (SELECT gid FROM fincas ORDER BY gid DESC LIMIT 1);

  FOR i IN SELECT * FROM json_array_elements(omgjson)
  LOOP

	insert into apartos(gidfinca,estado,geom,idactividad) values (lastReg,0,ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(''||i||''),4326),5367)),
	 1);
    
  END LOOP;
END;
$$ LANGUAGE plpgsql






/**********************************************************************************************************
PROCESO ALMACENADO PARA AÑADIR UN NUEVO APARTO O APARTOS(GEOJSON) A UNA FINCA X
**********************************************************************************************************/



CREATE OR REPLACE FUNCTION add_new_component(gidd int, datos varchar(50000)) RETURNS VOID AS $$
DECLARE
    omgjson json := datos ; --
    i json;
    toBuild geometry[];

    finalBuild geometry[];
    finalGeometry geometry;
    lastReg int;

    ax_add geometry;

    
BEGIN
	  FOR i IN SELECT * FROM json_array_elements(omgjson)
	  LOOP
		 toBuild = array_append(toBuild,(select ST_Multi(ST_GeomFromGeoJSON(''||i||''))));--array_append(toBuild,(select ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(''||i||''),4326),5367)))); 
	  END LOOP;


	--****************************************************CONVIERTE EL NUEVO AÑADIDO EN UN GEOMETRY
	ax_add = (select ST_Multi(ST_Collect(ST_Transform(ST_SetSRID(ST_Union(toBuild),4326),5367))));
	--*********************************************************************************************

	--***************************** PARA PODER GENERAR UN SOLO GEOMETRY SE NECESITA GUARDAR TANTO LA FINCA EN LA DB COMO EL GEOMETRY DEL NUEVO AÑADIDO
	finalBuild = array_append(finalBuild,ax_add);
	finalBuild = array_append(finalBuild,(SELECT geom FROM fincas WHERE gid=gidd));
	--*****************************

	-- select*from fincas where gid=23  select geom
	--********************************************************* UNE EL GEOMETRY OBTENIDO DEL NUEVO AÑADIDO Y EL DE LA FINCA EN LA BASE DE DATOS, PARA ARMAR UN SOLO GEOMETRY
	finalGeometry = (select ST_Multi(ST_Collect(ST_Union(finalBuild))));-- representa el nuevo geometry que va a sustituir la finca en la tabla fincas

	--*****************************************************************************************************************************


	--**************************************ACTUALIZA LA FINCA EN LA BASE DE DATOS
	

	UPDATE fincas SET geom = finalGeometry WHERE gid = gidd; -- select*from fincas  select*from apartos
	
	--****************************************************************************

	--***************************************************** ESTE CICLO SE ENCARGA DE INSERTAR LOS APARTOS O APARTOS EN LOS QUE SE COMPONGA EL NUEVO AÑADIDO, CON RESPECTO A LA FINCA QUE SE INDIQUE
	FOR i IN SELECT * FROM json_array_elements(omgjson)
	LOOP
		insert into apartos(gidfinca,estado,geom,idactividad) values (gidd,0,ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(''||i||''),4326),5367)),1);
	    
	END LOOP;	
	
	--**************************************************************************************************************************

END;
$$ LANGUAGE plpgsql




/***************************************************************
Proceso almacenado para sustituir en un aparto N divisiones
****************************************************************/

CREATE OR REPLACE  FUNCTION configurar_divisiones(idGid int, datos varchar(50000),idAparto int) RETURNS VOID AS $$
DECLARE
    omgjson json := datos ; --
    i json;
BEGIN

	-- ACTUALIZACIÓN DEL APARTO ANTERIOR  select*from apartos

	UPDATE apartos SET estado = 1 WHERE gid = idAparto AND gidfinca = idGid;

	FOR i IN SELECT * FROM json_array_elements(omgjson)
	LOOP

		insert into apartos(gidfinca,estado,geom,idactividad) values (idGid,0,ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(''||i||''),4326),5367)),
		1);
    
	END LOOP;
END;
$$ LANGUAGE plpgsql





--////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION insertarFinca(
	ARR_GEOMS TEXT[],
	idUser INT,
	FECHA VARCHAR(15),
	PROVINCIA VARCHAR(18),
	CANTON VARCHAR(15),
	DISTRITO VARCHAR(10),
	DIR_EXACTA VARCHAR(40),
	COD_FINCA  INT,
	TELEFONO   INT,
	NOMBRE_FINCA VARCHAR(15)

) RETURNS setof record 

AS
$insertar$
DECLARE
	CONT INT =0;
	LAST_ID INT;	
	r text;
BEGIN	
	insert into fincas (iduser,fecha,provincia,canton,distrito,direccionexacta,codigofinca,telefono,nombrefinca,geom)
		VALUES
			  (
			      idUser ,FECHA,PROVINCIA,CANTON,DISTRITO ,DIR_EXACTA,COD_FINCA,TELEFONO,NOMBRE_FINCA,
			      ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(ST_Union(ARR_GEOMS)),4326),5367))
			  );	
    LAST_ID = (SELECT gid from fincas  ORDER BY gid DESC LIMIT 1);
    FOR r IN  SELECT * FROM ARR_GEOMS
    WHERE CONT< array_length(ARR_GEOMS, int)
    LOOP
	-- select*from apartos

	INSERT INTO APARTOS(gidfinca,estado,geom,fecha,idactividad) values (LAST_ID,1,ST_Multi(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON(r),4326),5367),FECHA,1));
        RETURN NEXT r; 
    END LOOP;
END;
$insertar$
language plpgsql;








