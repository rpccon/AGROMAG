angular.module('AppPrueba')
.service('HistoricosService', ['$http','$q', function ($http, $q) {

    this.getFincas = function (idUser) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get('templates/historicos.module/historicos.logic.php?action=getFincasTodo&idUser='+idUser)
            .success(function(response) {
            defered.resolve(response);
        });

        return promise;
    };

    this.create_add_partition = function(id,stringWA,value,idAparto){
        var defered = $q.defer();
        var promise = defered.promise;


        var arrayToSend = {gid:id,stringWA:stringWA,idAparto:idAparto};



        $.ajax({
            type:"POST",
            data:arrayToSend,
            url: 'templates/historicos.module/historicos.logic.php?action=anadirGeo&value='+value,
            success: function (data) {
                defered.resolve({status:"success"});


            },
            error: function (textStatus, errorThrown) {
                defered.resolve({status:"error"});
            }

        });

        return promise;

    }


    this.createUnion = function(value){
        var defered = $q.defer();
        var promise = defered.promise;

        var json ={array:value};




        $http.get('templates/historicos.module/historicos.logic.php?action=createUnion&array='+value)
            .success(function(response) {
                defered.resolve(response);
            });




        return promise;
    }
    
    
    this.preview = function (gidFinca) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get('templates/historicos.module/historicos.logic.php?action=preview&gidFinca='+gidFinca)
            .success(function(response) {


                console.log("RESPONSE");
                console.log(response);
            defered.resolve(response);
        });

        console.log('PROMISE');
        console.log(promise);

        return promise;
    }
    
    this.getGeomHistorico = function (gidFinca,numHistorico) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get('templates/historicos.module/historicos.logic.php?action=history&gidFinca='+gidFinca+'&numHistorico='+numHistorico)
            .success(function(response) {
            defered.resolve(response);
        });

        return promise;
    }
    this.getHistAparto = function (gidAparto,anterior,gidFinca) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.get('templates/historicos.module/historicos.logic.php?action=histAparto&gidAparto='+gidAparto+'&anterior='+anterior+'&gidFinca='+gidFinca)
            .success(function(response) {
            defered.resolve(response);
        });

        return promise;
    }
    
    
    
    
}]);