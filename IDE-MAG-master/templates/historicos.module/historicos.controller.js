angular.module('AppPrueba')
.controller('HistoricosController', function ($scope,MostrarService,HistoricosService,VerEditarFormServiceCodigoFincaAparto,InsertarFormularioFincaxForm,$state) {
    $scope.fincas = [];

    $scope.gidFinca = VerEditarFormServiceCodigoFincaAparto.gidFinca;
    // $scope.apartoGid = VerEditarFormServiceCodigoFincaAparto.gidAparto;
    $scope.codigofinca = VerEditarFormServiceCodigoFincaAparto.codigofincaaparto;

    $scope.formularios = [];
    $scope.formulariosFincaAcual = [];
    $scope.respuestasFormActual = [];



    // Se debe de obtener el id del usuario
    $scope.idUser=1;
    HistoricosService.getFincas($scope.idUser).then(function (data) {
        console.log('Sending: '+$scope.idUser);
        $scope.fincas = data;
        console.log($scope.fincas)
    });


    $scope.openModal= function(){
       // $state.go('unir');

        console.log('lazos');
        $('#myModal').modal('show');

    };

    $scope.set_configuration_divs = function(){

        $("#set_divs").css('display','block');
        alertify.success('Seleccione el aparto que desea dividir');
        $scope.selectedApartos=[];
        $scope.change();

    };

    $scope.click_add_aparto = function(value){

        try {  // si value == 0 -> añadir // si no es un dividir

            if(value==0){
                getJsonObjectFromFile($scope.myFileX,value);
            }
            else{

                getJsonObjectFromFile($scope.myFileDiv,value);
            }

        }
        catch(err) {
            console.log('error');
            console.log(err);
        }
    };

    function setUpDataAdd(){
        getJsonObjectFromFile($scope.myFileX);
    }
    function getJsonObjectFromFile(file,value) {
        var reader = new FileReader();
        reader.onload = function(){
            var temp = JSON.parse(reader.result);


            var stringWithAll="'[";
            var arrayWithAll = [];

            for(var i=0;i< temp.features.length;i++){
                stringWithAll+=(JSON.stringify(temp.features[i].geometry));
                if(i!=temp.features.length-1){
                    stringWithAll+=",";
                }
            }
            stringWithAll+="]'";


                var idAparto = 0;
                if(value==1){
                    idAparto = parseInt($scope.selectedApartos[0]);
                }

                HistoricosService.create_add_partition($scope.gidFinca,stringWithAll,value,idAparto).then(function (data) {


                    if(value==1){
                        alertify.success('División realizada satisfactoriamente');
                        $scope.change();
                        $scope.selectedApartos=[];
                        $('#myModalDiv').modal('hide');
                    }
                    else{

                        alertify.success('Añadido realizado exitosamente');
                        $scope.change();
                        $scope.selectedApartos=[];
                        $('#myModal').modal('hide');
                    }


                }, function(error){

                    $scope.change();
                    alertify.error('Error, en operación');
                    $scope.selectedApartos=[];
                });





        };
        reader.readAsText(file);


    }

    $scope.config_divs = function (){

        if($scope.selectedApartos.length==0){
            alertify.error('Error, debe seleccionar un aparto para realizar el proceso de división');
        }
        else if($scope.selectedApartos.length==2){
            alertify.error('Error, debe seleccionar únicamente un aparto. (Para desseleccionar los apartos, oprima nuevamente el botón de dividir)');
        }
        else{

            $('#myModalDiv').modal();
        }

    };

    $scope.config_union = function(){


        if($scope.selectedApartos==[]){
            alertify.success('Error, debe seleccionar los apartos que desea unir');
        }
        else if($scope.selectedApartos.length==1){
            alertify.success('Error, debe seleccionar mas de un aparto para unir');
        }
        else{
            var finalArray = new Array();

            for(var i=0;i<$scope.selectedApartos.length;i++){
                finalArray.push(parseInt($scope.selectedApartos[i]));
            }
            var lastString= "'"+ JSON.stringify(finalArray)+"'";


            HistoricosService.createUnion(lastString).then(function (data) {
                $scope.change();
                alertify.success('Actualización exitosa');
                $scope.selectedApartos=[];
            }, function(error){
                alert('Error');
                $scope.change();
                alertify.error('Error en la operación');
                $scope.selectedApartos=[];
            });
        }

    }


    // CUANDO SE SELECCIONA ALGUNA FINCA EN EL COMBOBOX
    $scope.change = function(){

        HistoricosService.preview($scope.gidFinca).then(function (data) {
            console.log($scope.gidFinca);
            console.log('Ojoooooo');
            console.log(data);
            $scope.numHistoricoActual = data.max;
            $scope.Max = data.max;
            console.log('Lo que hace la varita');
            console.log(data.finca);
            reconvertJsonPolygon(data.finca,false);
        });


        MostrarService.getFincasByID("1", $scope.gidFinca).then(function(data){
            $scope.dataFinca = data[0];
            console.log($scope.dataFinca);

            $scope.codigofinca = $scope.dataFinca.codigofinca;

            //Para enviar al mostrarRespuesta el tipo
            VerEditarFormServiceCodigoFincaAparto.codigofincaaparto = $scope.codigofinca;
            VerEditarFormServiceCodigoFincaAparto.tipo = "finca";
            VerEditarFormServiceCodigoFincaAparto.gidFinca = $scope.gidFinca;
            VerEditarFormServiceCodigoFincaAparto.origen = "historicos";

            //Para enviar  a la vista de mostrar
            InsertarFormularioFincaxForm.idFincaxFormulario = $scope.gidFinca;
            // Aqui se actializa la lista de formularios disponibles para la finca seleccionada
            $scope.apartoAtual = false;

        });
    }


    $scope.anadirFormAparto = function () { //funciona bien

        if($scope.formActualAparto == undefined || $scope.formActualAparto == "")
        {

        }
        else {
            MostrarService.insertarFormAparto($scope.formActualAparto,$scope.apartoGid).then(function (data) {
            });
            $scope.formActualAparto = "";
            $scope.actualizarlistaFormAparto();
        }
    }


    $scope.actualizarlistaFormAparto = function () {

        MostrarService.getFormulariosFinca($scope.apartoGid,"aparto").then(function (data) {
            console.log(data);
            if(data != "false") {
                $scope.formulariosApartoActual = data;
            }
            else {
                $scope.formulariosApartoActual = [];
            }


        });
        //Aqui se actualiza la lista de formularios ya añadidos aL aparto
        MostrarService.getFormulariosNoFinca($scope.apartoGid,"aparto").then(function (data) {
            $scope.formularios = data;
        });
    }

    $scope.formActualFuncAparto = function(elem){
        $scope.respActualAparto =  JSON.parse(elem);
    }

    $scope.mostrarRespuestasFormAparto = function () {
        if ($scope.respActualAparto!= "" )
        {

            VerEditarFormServiceCodigoFincaAparto.respuesta = $scope.respActualAparto;
            VerEditarFormServiceCodigoFincaAparto.nombrefinca = $scope.dataFinca.nombrefinca;
            VerEditarFormServiceCodigoFincaAparto.nombrepropietario = $scope.dataFinca.nombreprop;
            VerEditarFormServiceCodigoFincaAparto.apellidosPropietario = $scope.dataFinca.apellidos;
            $state.go('dashboard.verRespForm');
        }


    }




    $scope.json = [];
    function reconvertJsonPolygon(puntos,aparto) {
        var json = [];
        var points = '';
        var clicked=0

        for(var i = 0; i < puntos.length; i++) {
            for (var j = 0; j < puntos[i].puntos.length; j++) {
                points += puntos[i].puntos[j].x + ',' + puntos[i].puntos[j].y + ' ';
            }
            json.push({gid:puntos[i].gid,puntos: points.slice(0, points.length-1),clicked:0});
            points = '';
        }
        if(!aparto)
            $scope.json = json;
        else
            $scope.jsonSeleccionado=json;
    }


    $scope.tSeleccion ="null";
    $scope.selectedApartos=[];
    $scope.findPoints=false;

    function look(val){

        console.log('val');
        console.log(val);
        var find=false;
        for(var i=0;i<$scope.selectedApartos;i++){
            if($scope.selectedApartos[i]==val){
                find=true;
                break;
            }
        }
        return find;
    }
    $("#unir").click(function(){
        if($scope.gidFinca===""){
            alert("Error, debe seleccionar una finca");
        }
        else{
            $scope.selectedApartos=[];
            // -------------------------------------------
            $('#infoForms').css('color', 'black');
            $('#infoForms').css('background-color', 'white');

            $('#anadir').css('color', 'black');
            $('#anadir').css('background-color', 'white');

            $('#dividir').css('color', 'black');
            $('#dividir').css('background-color', 'white');
            //--------------------------------------------

            $('#unir').css('color', 'white');
            $('#unir').css('background-color', 'blue');


            $('#second_div').css('display', 'none');
            $('#set_divs').css('display', 'none');



            $('#set_union').css('display', 'block');
            $('#set_union').css('margin-top', '100px');
            $('#buttons_pass').css('display', 'none');
            //$('#tam_tab').css('height', '100px');
           // $('#message_info').text('Union');
           // $('#message_info2').css('display', 'none');
            $('#show_id').css('display', 'none');


            $scope.tSeleccion="unir";
        }



    });

    $("#infoForms").click(function(){
        $scope.tSeleccion=="infoForms";



        console.log('here');
       // console.log();


        $scope.jsonSeleccionado=[];

        if($scope.gidFinca===""){
            alert("Error, debe seleccionar una finca");
        }
        else{
            for(var i=0;i<$scope.json.length;i++){
                i
                    $scope.json[i].clicked=0;

            }


            $('#unir').css('color', 'black');
            $('#unir').css('background-color', 'white');

            $('#anadir').css('color', 'black');
            $('#anadir').css('background-color', 'white');

            $('#dividir').css('color', 'black');
            $('#dividir').css('background-color', 'white');
            //--------------------------------------------


            $('#infoForms').css('color', 'white');
            $('#infoForms').css('background-color', 'blue');


            $('#second_div').css('display', 'block');

            $('#set_union').css('display', 'none');
            $('#buttons_pass').css('display', 'block');
          //  $('#message_info').text('Información general');
           // $('#message_info2').css('display', 'block');
            $('#show_id').css('display', 'block');

            $scope.tSeleccion="infoForms";
        }
        // -------------------------------------------


    });

    $("#anadir").click(function(){
        // -------------------------------------------
        $('#unir').css('color', 'black');
        $('#unir').css('background-color', 'white');

        $('#infoForms').css('color', 'black');
        $('#infoForms').css('background-color', 'white');

        $('#dividir').css('color', 'black');
        $('#dividir').css('background-color', 'white');
        //--------------------------------------------


        $('#anadir').css('color', 'white');
        $('#anadir').css('background-color', 'blue');

        $scope.tSeleccion="anadir";

    });
    $("#dividir").click(function(){
        // -------------------------------------------
        $('#unir').css('color', 'black');
        $('#unir').css('background-color', 'white');

        $('#infoForms').css('color', 'black');
        $('#infoForms').css('background-color', 'white');

        $('#anadir').css('color', 'black');
        $('#anadir').css('background-color', 'white');
        //--------------------------------------------


        $('#dividir').css('color', 'white');
        $('#dividir').css('background-color', 'blue');


        $('#second_div').css('display', 'none');
        $('#set_union').css('display', 'none');


        $('#set_divs').css('display', 'block');
        $('#set_divs').css('margin-top', '100px');
        $('#buttons_pass').css('display', 'none');

        $scope.tSeleccion="unir";

    });
    //------------------------------------------
    $('#unir').css('color', 'black');
    $('#unir').css('background-color', 'white');

    $('#anadir').css('color', 'black');
    $('#anadir').css('background-color', 'white');

    $('#dividir').css('color', 'black');
    $('#dividir').css('background-color', 'white');
    //--------------------------------------------


    $('#infoForms').css('color', 'white');
    $('#infoForms').css('background-color', 'blue');

    $scope.tSeleccion="infoForms";
    //-------------------------------------------


    $scope.gidAparto = "";
    $scope.jsonSeleccionado=[];


    $scope.updatePanelMap =function(){
        if($scope.tSeleccion=="unir"){
            console.log("mamé")
        }
        else{
            console.log("sale");
        }
    }

    $scope.unir = function(gid, coordenadas){
        if($scope.tSeleccion=="unir"){
            for(var i=0;i<$scope.json.length;i++){
                if($scope.json[i].puntos==coordenadas){
                    $scope.json[i].clicked=1;
                }
            }
        }







        VerEditarFormServiceCodigoFincaAparto.codigofincaaparto = gid;
        VerEditarFormServiceCodigoFincaAparto.tipo = "aparto";
        VerEditarFormServiceCodigoFincaAparto.gidAparto = gid;


        $scope.selectedApartos.push(gid);

           console.log(gid);
        $scope.apartoGid=gid;
        $scope.jsonSeleccionado=[];


            $scope.jsonSeleccionado.push({id:gid,puntos:coordenadas});




            MostrarService.getApartoByID($scope.apartoGid,$scope.gidFinca).then(function(data){
                $scope.dataAparto = data[0];
                $scope.actualizarlistaFormAparto();
            });

        if($scope.tSeleccion!="unir"){
            $scope.apartoAtual = true;
        }


    }

    $scope.siguienteFinca = function(){
        if($scope.numHistoricoActual+1 > 0 && $scope.numHistoricoActual+1 <= $scope.Max)
        {      
            $scope.numHistoricoActual+=1;
            getHistorico();
        }
    }
    
    $scope.AnteriorFinca = function(){
        if($scope.numHistoricoActual-1 > 0 && $scope.numHistoricoActual-1 <= $scope.Max)
        {      
            $scope.numHistoricoActual-=1;
            getHistorico();
        }
    }


    var getHistorico = function()
    {
        HistoricosService.getGeomHistorico($scope.gidFinca,$scope.numHistoricoActual).then(function (data) {
            reconvertJsonPolygon(data,false);
        });
    }
    
    $scope.siguienteAparto= function(){
        
        HistoricosService.getHistAparto($scope.apartoGid,0,$scope.gidFinca).then(function(data)
        {
            console.log(data);
            $scope.apartoGid= data[0].gid;
            reconvertJsonPolygon(data,true);
        });
    }
    
    $scope.AnteriorAparto = function(){
        HistoricosService.getHistAparto($scope.apartoGid,1,$scope.gidFinca).then(function(data)
        {
            console.log(data[0].gid);
            $scope.apartoGid= data[0].gid;
            reconvertJsonPolygon(data,true);
        });
    }
    $scope.hist=function(gid)
    {
        console.log(gid);
        $scope.apartoGid= gid;
    }
    
    

    });
